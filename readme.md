### run migration

```mvn flyway:migrate```

### build for prod

``` mvn -Pprod clean install```

### build for dev

``` mvn -Pdev clean install```

if you want to set the profile after the code is built, we can use a java vm management at application launch. This is done as follows:

```java -jar -Dspring.profiles.active=prod app.jar```

