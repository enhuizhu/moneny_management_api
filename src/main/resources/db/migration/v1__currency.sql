CREATE TABLE IF NOT EXISTS currency (
  id int(20) NOT NULL AUTO_INCREMENT,
  name varchar(255),
  PRIMARY KEY (id)
);

ALTER TABLE user ADD currency_id int(20) AFTER password;

INSERT INTO currency (name) values ('rmb');
INSERT INTO currency (name) values ('gbp');
