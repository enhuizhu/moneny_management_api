package money.management.daos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;

@Entity
@Table(name="currency")
public class Currency {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
  private String name;

  // @OneToMany(mappedBy = "currency")
  // private User user;
  
  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  public int getId() {
    return this.id;
  }
}
