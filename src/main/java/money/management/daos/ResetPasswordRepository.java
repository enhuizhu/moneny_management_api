package money.management.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ResetPasswordRepository extends JpaRepository <ResetPassword, Integer> {
	@Query("from ResetPassword r where r.token = :token")
	public ResetPassword findByToken(@Param("token") String token);
}
