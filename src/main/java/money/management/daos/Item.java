package money.management.daos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Entity
@Table(name="item")
public class Item {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String name;
	private Date createdAt;
	private Date updatedAt;
	private final static Logger LOGGER = Logger.getLogger(Item.class.getName());
	
	@OneToMany(mappedBy="item", cascade = CascadeType.ALL)
	Set<Cost> costs = new HashSet<Cost>();
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;
	
	public int getId() {
		return this.id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Date getCreatedAt() {
		return this.createdAt;
	}
	
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public Date getUpdatedAt() {
		return this.updatedAt;
	}
	
	public void addCost(Cost cost) {
		this.costs.add(cost);
		cost.setItem(this);
	}

	public float getTotal() {
		float total = 0;
		
		for(Cost c : this.costs) {
			total += c.getValue();
		}

		return total;
	}

	public List<Cost> costs() {
		List<Cost> newCosts = this.costs.stream().collect(Collectors.toList());
		Collections.sort(newCosts, (o1, o2) -> o2.getId() - o1.getId());
		return newCosts;
	}

	public List<Cost> costs(Date startDate, Date endDate) {
		List<Cost> newCosts = this.costs().stream().filter((Cost c) -> {
			return c.getPayDate().compareTo(startDate) >= 0 
				&& c.getPayDate().compareTo(endDate) <=0;
		}).collect(Collectors.toList());

		Collections.sort(newCosts, (o1, o2) -> o2.getId() - o1.getId());
		
		return newCosts;
	}
	
	public void setUser(User user) {
		this.user = user;	
	}
}
