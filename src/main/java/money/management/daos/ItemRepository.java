package money.management.daos;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Sort;

public interface ItemRepository extends JpaRepository<Item, Integer> {
  // public Set<Item> getItemsOrderById(int id, Sort)
}
