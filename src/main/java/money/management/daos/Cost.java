package money.management.daos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import java.util.Date;

@Entity
@Table(name="cost")
public class Cost {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private Date createdAt;
	private Date updatedAt;
	private Date payDate;
	private float value;
	private String comment;
	
	@ManyToOne
  @JoinColumn(name="item_id")
  private Item item;
	
	public void setValue(float value) {
		this.value = value;
	}
	
	public float getValue() {
		return this.value;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getComment() {
		return this.comment;
	}

	public int getId() {
		return this.id;
	}
	
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public Date getCreatedAt() {
		return this.createdAt;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	
	public Date getPayDate() {
		return this.payDate;
	}
	
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public Date getUpdatedAt() {
		return this.updatedAt;
	}
	
	public String getItem() {
		return this.item.getName();
	}
	
	public void setItem(Item item) {
		this.item = item;
	}
}
