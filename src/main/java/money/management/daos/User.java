package money.management.daos;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String email;
	private String password;
	
	@OneToMany(mappedBy="user", cascade = CascadeType.ALL)
	Set<Item> items = new HashSet<Item>();

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "currency_id", referencedColumnName = "id")
	private Currency currency;

	public Integer getId() {
		
		return id;
	}
	
	public String getEmail() {
		return this.email;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Currency getCurrency() {
		return this.currency;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addItem(Item item) {
		this.items.add(item);
		item.setUser(this);
	}

	public List<Item> getItems() {
		List<Item> newItems = this.items.stream().collect(Collectors.toList());
		Collections.sort(newItems, (o1, o2) -> o2.getId() - o1.getId());
		return newItems;
	}
}
