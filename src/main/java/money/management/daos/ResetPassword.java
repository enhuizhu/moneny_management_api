package money.management.daos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ResetPassword {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String email;
	private String token;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getToken() {
		return this.token;
	}
}
