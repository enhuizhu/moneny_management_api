package money.management.daos;

import java.util.Set;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository <User, Integer> {
  @Query("from User u where u.email = :email")
  public User findByMail(@Param("email") String email);
}
