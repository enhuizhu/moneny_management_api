package money.management.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.jwt.codec.Codecs;
import org.springframework.security.jwt.crypto.cipher.CipherMetadata;
import org.springframework.security.jwt.crypto.sign.Signer;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.SignatureAlgorithm;
import money.management.daos.User;
import money.management.daos.UserRepository;

import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class TokenService {
	@Autowired
	private UserRepository userDao;
	
	public static final int TOKEN_OK = 1;
	public static final int TOKEN_EXPIRED = 2;
	public static final int INVALID_TOKEN = 3;
	public static final String SECRET_KEY = "oeRaYY7Wo24sDqKSX3IM9ASGmdGPmkTd9jo1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI-ZQ19k_o9MI0y3eZN2lp9jow55FfXMiINEdt1XR85VipRLSOkT6kSpzs2x-jbLDiz9iFVzkd81YKxMgPA7VfZeQUm4n-mOmnWMaVX30zGFU4L3oPBctYKkl4dYfqYWqRNfrgPJVi5DGFjywgxx0ASEiJHtV72paI3fDR2XwlSkyhhmY-ICjCRmsJN4fX1pdoL8a18-aQrvyu4j0Os6dVPYIoPvvY0SAZtWYKHfM15g7A3HD4cVREf9cUsprCRK93w";
	public static final String ISSUER = "olmarket";
	public static final String ID = "olmarket";
	public static final long  TTMILLIS = 1000 * 60 * 60 * 24; // one day
	
	public String generateToekn(String email) {
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        
        JwtBuilder builder = Jwts.builder().setId(ID)
	        .setIssuedAt(now)
            .setSubject(email)
            .setIssuer(ISSUER)
            .signWith(signatureAlgorithm, signingKey);
        
        //if it has been specified, let's add the expiration
        if (TTMILLIS >= 0) {
            long expMillis = nowMillis + TTMILLIS;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }
        
        return builder.compact();
	}

	public User getUserViaToken(String token) {
		Claims claims = decodeJWT(token);	
		String subject = claims.getSubject();		
		User user = this.userDao.findByMail(subject);
		return user;
	}
	
	public int validateToken(String token) {
		Claims claims;
		try {
			claims = decodeJWT(token);	
		} catch(ExpiredJwtException e) {
			return TOKEN_EXPIRED;
		}
		
		String subject = claims.getSubject();		
	
		User user = this.userDao.findByMail(subject);
		
		if (user == null) {
			return INVALID_TOKEN;
		}
		
		// need to check if token expired
		Date expireDate = claims.getExpiration();
		Date now = new Date(System.currentTimeMillis());
		
		if (expireDate.compareTo(now) < 0) {
			return TOKEN_EXPIRED;
		}
		
		return TOKEN_OK;
	}
	
	public Claims decodeJWT(String jwt) {
        //This line will throw an exception if it is not a signed JWS (as expected)
			Claims claims = Jwts.parser()
					.setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
					.parseClaimsJws(jwt).getBody();
			return claims;
	}
}
