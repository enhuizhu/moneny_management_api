package money.management.services;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordService {
	private static PasswordService instance = null;
	public BCryptPasswordEncoder encoder;
	
	public PasswordService() {
		encoder = new BCryptPasswordEncoder();
	}
	
	public static PasswordService getInstance() {
		if (instance == null) {
			instance = new PasswordService();
		}
		
		return instance;
	}
	
	public String encode(String pass) {
		return encoder.encode(pass);
	}
	
	public Boolean matches(String rawPassword, String encodedPassword) {
		return encoder.matches(rawPassword, encodedPassword);
	}
	
}
