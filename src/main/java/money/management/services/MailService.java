package money.management.services;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;

@Service
public class MailService {
	@Autowired
  Environment environment;
	
	public void send(String to, String subject, String content) throws MessagingException {
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");     
		properties.setProperty("mail.host", "smtp.mail.yahoo.com");  
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", "465");  
		properties.put("mail.debug", "true");  
		properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
		properties.put("mail.smtp.socketFactory.fallback", "false"); 
		
		Session session = Session.getDefaultInstance(properties,
			new javax.mail.Authenticator() {  
		      	protected PasswordAuthentication getPasswordAuthentication() {  
		    	    return new PasswordAuthentication("enhui.zhu@yahoo.com", "sjgimo853");  
		    	}  
		     }
		);
		
		MimeMessage message = new MimeMessage(session);  
		message.setFrom(new InternetAddress("enhui.zhu@yahoo.com"));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject(subject);
		message.setText(content);
		
		Transport transport = session.getTransport("smtp");
		transport.send(message, message.getAllRecipients());
		transport.close();
	}	
}
