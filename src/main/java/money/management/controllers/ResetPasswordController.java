package money.management.controllers;

import java.util.UUID;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import money.management.daos.ResetPassword;
import money.management.daos.ResetPasswordRepository;
import money.management.daos.User;
import money.management.daos.UserRepository;
import money.management.libs.ApiResponse;
import money.management.services.MailService;
import money.management.services.PasswordService;

@Controller
@RequestMapping(path = "reset-password")
public class ResetPasswordController {
	@Value("${frontend.ui.url}")
	private String frontendUrl;
	
	@Autowired
	UserRepository users;
	
	@Autowired
	ResetPasswordRepository resetPasswordRepository;

	@Autowired
	MailService mailService;
	
	@RequestMapping(method = RequestMethod.POST, path = "/generate-link")
	public @ResponseBody ApiResponse<String> generateLink(@RequestBody ResetPassword reset) {
		String email = reset.getEmail();
		
		// should check if email inside db
		User user = users.findByMail(email);
		
		if (user == null) {
			return new ApiResponse<String>("user with email " + email + " does not exist.", false);
		}
		
		// should generate unique id
		UUID uuid = UUID.randomUUID();
		
		String randomUUIDString = uuid.toString();
		
		reset.setToken(randomUUIDString);
		resetPasswordRepository.save(reset);
		
		String link = this.frontendUrl + "/set-new-password/?token=" + uuid;
		
		try {
			this.mailService.send(user.getEmail(), 
				"Reset Password", 
				"hi, here is the link for resetting passowrd " + link);	
		} catch(MessagingException e) {
			return new ApiResponse<String>(e.getMessage(), false);	
		}
	    
		return new ApiResponse<String>("reset password link has been sent to your email, please check your email!", true);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "set-new-password")
	public @ResponseBody ApiResponse<String> setNewPassword(@RequestBody ResetPasswordPayload payload)
	{
		
		String newPass = payload.newPass;
		String newPassDup = payload.newPassDup;
		
		if (!newPass.equals(newPassDup)) {
			return new ApiResponse<String>("Two passwords don't match eatch other.", false);
		}
		
		String token = payload.token;
		
		ResetPassword reset =  resetPasswordRepository.findByToken(token);
		
		if (reset == null) {
			return new ApiResponse<String>("Can not find token " + token, false);
		}
		
		String email = reset.getEmail();
		
		User user = users.findByMail(email);
		
		if (user == null) {
			return new ApiResponse<String>("can not find user with email " + email, false);
		}
		
		user.setPassword(PasswordService.getInstance().encode(newPass));
		users.save(user);
		resetPasswordRepository.delete(reset);
		
		return new ApiResponse<String>("password has been reset successfully", true);
	}	
}

class ResetPasswordPayload {
	public String newPass;
	public String newPassDup;
	public String token;
}

