package money.management.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import money.management.daos.Currency;
import money.management.daos.CurrencyRepository;
import money.management.libs.ApiResponse;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.spi.CurrencyNameProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(path = "currency") 
public class CurrencyController {
  Logger LOG = LoggerFactory.getLogger(CurrencyController.class);

  @Autowired
  CurrencyRepository currencyDao;

  @RequestMapping(value="/create-currency", method=RequestMethod.POST)
  public @ResponseBody ApiResponse<String> createCurrency(@RequestBody Currency currency) {
    if (currency.getName() == null) {
      return new ApiResponse<String>("the name of currency can not be empty", false);
    }
    
    currencyDao.save(currency);
    
    return new ApiResponse<String>("new currency has been created successfully", true);
  }

  @RequestMapping(value = "/get-currencies", method = RequestMethod.GET)
  public @ResponseBody ApiResponse<List<Currency>> getCurrency() {
    return new ApiResponse<List<Currency>>(currencyDao.findAll(), true);
  }
}