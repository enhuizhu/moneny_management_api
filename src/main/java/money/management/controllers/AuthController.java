package money.management.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import money.management.libs.ApiResponse;
import money.management.daos.Currency;
import money.management.daos.User;
import money.management.daos.UserRepository;
import money.management.services.PasswordService;
import money.management.services.TokenService;
import money.management.daos.CurrencyRepository;;

@Controller
@RequestMapping(path="auth")
public class AuthController {
	@Autowired
  private UserRepository userDao;
  
  @Autowired
  public CurrencyRepository currencyDao;
  
  @Autowired
  private TokenService tokenService;
  
  @RequestMapping(method = RequestMethod.POST, path="/login") 
  public @ResponseBody ApiResponse<String> login(@RequestBody User loginUser) {
    String email = loginUser.getEmail();
    String password = loginUser.getPassword();
    
    User user = this.userDao.findByMail(email);
    
    if (user == null) {
      return new ApiResponse<String>(email + " does not exist!", false);
    }
    
    if (PasswordService.getInstance().matches(password, user.getPassword())) {
      Currency currency = this.getUserCurrency(user);
      String jsonString = new JSONObject()
        .put("token", tokenService.generateToekn(email))
        .put("email", email)
        .put("currency", 
          new JSONObject()
            .put("id", currency.getId())
            .put("name", currency.getName())
        ).toString();
      
      return new ApiResponse<String>(jsonString, true);	
    }
    
    return new ApiResponse<String>("password is wrong", false);
  }

  Currency getUserCurrency(User user) {
    Currency currency = user.getCurrency();
    
    if (currency == null) {
      List<Currency> currencies = currencyDao.findAll();

      if (currencies.size() <= 0) {
        return currency;
      }

      user.setCurrency(currencies.get(0));
      userDao.save(user);

      return currencies.get(0);
    }

    return currency;
  }
}
