package money.management.controllers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.json.*;
import javax.json.stream.JsonParser;
import javax.mail.MessagingException;

import money.management.services.PasswordService;
import money.management.services.TokenService;
import money.management.services.MailService;
import money.management.libs.ApiResponse;
import money.management.daos.Currency;
import money.management.daos.CurrencyRepository;
import money.management.daos.User;
import money.management.daos.UserRepository;


@Controller
@RequestMapping(path = "user")
public class UserController {
	public final static String TOKEN = "token";

	@Value("${frontend.ui.url}")
	private String frontendUrl;

	@Autowired
	private UserRepository userDao;

	@Autowired
	private CurrencyRepository currencyDao;

	@Autowired
	private MailService mailService;

	@Autowired
	TokenService tokenService;

	@RequestMapping(method = RequestMethod.POST, path = "/create")
	public @ResponseBody ApiResponse<String> create(@RequestBody User newUser)
			throws InterruptedException, ExecutionException {
		User user = this.userDao.findByMail(newUser.getEmail());
		
		if (user != null) {
			return new ApiResponse<String>("user already exist", false);
		}
		
		newUser.setPassword(PasswordService.getInstance().encode(newUser.getPassword()));
		
		this.userDao.save(newUser);

		try {
			this.mailService.send(newUser.getEmail(), 
				"New User Registration In Finance Management", 
				"hi," + newUser.getEmail() + "\n" 
				+ "Thanks for registering in Finance Management" + "\n"
				+ "Please this url:" + this.frontendUrl +" to access the App" + "\n"
			);
		} catch(MessagingException e) {
			return new ApiResponse<String>(e.getMessage(), false);	
		}
		
		return new ApiResponse<String>("user has been created successfully!", true);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/setCurrency")
	public @ResponseBody ApiResponse<String> setCurrency(
		@RequestHeader(TOKEN) String token,
		@RequestParam int currency_id
	) {
		Currency currency = currencyDao.findById(currency_id).orElse(null);

		if (currency == null) {
			return new ApiResponse<String>("can not find currency with id: " + currency_id, false);
		}

		User user = tokenService.getUserViaToken(token);

		if (user == null) {
			return new ApiResponse<String>("can not find user with token " + token , false);
		}

		// everything is good
		user.setCurrency(currency);

		userDao.save(user);
		return new ApiResponse<String>("User currency has been setup successfully!", true);
	}
}
