package money.management.controllers;

import java.util.Date;
import java.util.Optional;

import javax.json.Json;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import money.management.daos.Cost;
import money.management.daos.CostRepository;
import money.management.daos.Item;
import money.management.daos.ItemRepository;
import money.management.libs.ApiResponse;

@Controller
@RequestMapping(path = "data/cost")
public class CostController {
  @Autowired
  private CostRepository costDao;

  @Autowired
	private ItemRepository itemDao;
  
  @RequestMapping(method = RequestMethod.POST, path = "/create-cost")
	public @ResponseBody ApiResponse<String> createCost(
    @RequestBody String newCostStr,
    @RequestParam int itemId
	) {
    Gson g = new Gson();
    Cost newCost = g.fromJson(newCostStr, Cost.class);
    
    Date now = new Date();
    newCost.setCreatedAt(now);
    newCost.setUpdatedAt(now);

    try {
      Item item = itemDao.findById(itemId).orElse(null);

      if (item == null) {
        return new ApiResponse<String>("can not find item with item id:" + itemId, false);
      } else {
        item.addCost(newCost);
        newCost.setItem(item);
        costDao.save(newCost);
        return new ApiResponse<String>("cost has been created successfully.", true);
      }
    } catch(Exception e) {
      return new ApiResponse<String>(e.getMessage(), false); 
    }
  }
  
  @RequestMapping(method = RequestMethod.DELETE, path = "delete-cost")
  public @ResponseBody ApiResponse<String> deleteCost(@RequestParam int costId) {
    Cost cost = costDao.findById(costId).orElse(null);

    if (cost == null) {
      return new ApiResponse<String>("can not find cost with cost id:" + costId, false);
    } else {
      costDao.delete(cost);
      return new ApiResponse<String>("cost has been deleted successfully.", true);
    }
  }

  @RequestMapping(method = RequestMethod.POST, path = "update-cost")
  public @ResponseBody ApiResponse<String> updateCost(
    @RequestBody String newCostStr,
    @RequestParam int costId
  ) {
    Cost cost = costDao.findById(costId).orElse(null);

    if (cost == null) {
      return new ApiResponse<String>("can not find cost with cost id:" + costId, false);
    } else {
      Gson g = new Gson();
      Cost newCost = g.fromJson(newCostStr, Cost.class);
      
      Date now = new Date();
      newCost.setUpdatedAt(now);
      
      if (newCost.getValue() != 0L) {
        cost.setValue(newCost.getValue());
      }

      if (newCost.getPayDate() != null) {
        cost.setPayDate(newCost.getPayDate());
      }

      if (newCost.getComment() != null) {
        cost.setComment(newCost.getComment());
      }

      costDao.save(cost);

      return new ApiResponse<String>("cost has been updated successfully.", true);
    }
  }
}