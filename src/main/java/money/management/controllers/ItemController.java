package money.management.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.json.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import money.management.libs.ApiResponse;
import money.management.services.TokenService;
import money.management.daos.*;

@Controller
@RequestMapping(path = "data/item")
public class ItemController {
	public final static String TOKEN = "token";
	Logger LOG = LoggerFactory.getLogger(ItemController.class);

	@Autowired
	TokenService tokenService;

	@Autowired
	private ItemRepository itemDao;

	@RequestMapping(method = RequestMethod.POST, path = "/create-item")
	public @ResponseBody ApiResponse<String> createItem(@RequestBody Item newItem, @RequestHeader(TOKEN) String token) {
		Date now = new Date();
		newItem.setUpdatedAt(now);
		newItem.setCreatedAt(now);

		User user = tokenService.getUserViaToken(token);
		newItem.setUser(user);
		itemDao.save(newItem);

		return new ApiResponse<String>("new item has been added", true);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/item-cost")
	public @ResponseBody ApiResponse<List<Cost>> getItemCosts(@RequestParam int id) throws Exception {
		Item item = itemDao.findById(id).orElse(null);

		if (item == null) {
			throw new Exception("can not find item with item id:" + id);
		} else {
			return new ApiResponse<List<Cost>>(item.costs(), true);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/item-total-cost")
	public @ResponseBody ApiResponse<Float> getTotalInDateRange(@RequestParam int id,
			@RequestBody String requestBody) throws Exception {
		Item item = itemDao.findById(id).orElse(null);

		if (item == null) {
			throw new Exception("can not find item with item id:" + id);
		} else {

			List<Cost> costs = this.getCostsBaseOnRequestBody(requestBody, id);

			float total = 0;

			for (Cost c : costs) {
				total += c.getValue();
			}

			return new ApiResponse<Float>(total, true);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path = "/item-cost")
	public @ResponseBody ApiResponse<List<Cost>> getItemCostsInDateRange(
			@RequestParam int id,
			@RequestBody String requestBody) throws Exception {
		Item item = itemDao.findById(id).orElse(null);

		if (item == null) {
			throw new Exception("can not find item with item id:" + id);
		} else {
			List<Cost> costs = this.getCostsBaseOnRequestBody(requestBody, id);
			return new ApiResponse<List<Cost>>(costs, true);
		}
	}

	public List<Cost> getCostsBaseOnRequestBody(String requestBody, int id) throws ParseException, JSONException {
		JSONObject jsonObject = new JSONObject(requestBody);
		Date startDate =  new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.get("startDate").toString());
		Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(jsonObject.get("endDate").toString());
		Item item = itemDao.findById(id).orElse(null);
		return item.costs(startDate, endDate);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/get-items")
	public @ResponseBody ApiResponse<List<Item>> getItems(
		@RequestHeader(TOKEN) String token) {
		User user = tokenService.getUserViaToken(token);
		List<Item> results = user.getItems();

		return new ApiResponse<List<Item>>(results, true);
	}

	@RequestMapping(method = RequestMethod.DELETE, path = "/delete-item")
	public @ResponseBody ApiResponse<String> deleteItem(
		@RequestParam int id) {
		
		Item item = itemDao.findById(id).orElse(null);

		if (item != null) {
			itemDao.deleteById(id);
			return new ApiResponse<String>("item has been deleted!", true);
		} else {
			return new ApiResponse<String>(String.format("can not find item with id: %d", id), true);
		}
	}

	@RequestMapping(method = RequestMethod.POST, path= "update-item")
	public @ResponseBody ApiResponse<String> updateItem(
		@RequestBody Item newItem,
		@RequestParam int id
	) {
		Item item = itemDao.findById(id).orElse(null);

		if (item == null) {
			return new ApiResponse<String>(String.format("can not find item with id: %d", id), true);
		} else {
			if (newItem.getName().trim() != "") {
				item.setName(newItem.getName());
				Date now = new Date();
				item.setUpdatedAt(now);
				itemDao.save(item);
				return new ApiResponse<String>("item has been updated successfully.", true);
			} else {
				return new ApiResponse<String>("name if item can not be empty", false);
			} 
		}
	}
}
