package money.management.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path="env")
public class EnvController {
  @Value("${frontend.ui.url}")
  private String frontendUrl;
  
  @RequestMapping(method = RequestMethod.GET, path = "/output") 
  public @ResponseBody String outputUrl() {
    return this.frontendUrl;
  }
}