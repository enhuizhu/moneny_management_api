package money.management.libs;

public class ApiResponse<T> {
	public boolean success;
	public T data;
	
	public ApiResponse(T data, boolean success) {
		this.data = data;
		this.success = success;
	}
}
