package money.management.interfaces;

public interface ResetPasswordPayloadInterface {
	public String newPass = null;
	public String newPassDup = null;
	public String token = null;
}
