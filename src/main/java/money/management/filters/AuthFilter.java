package money.management.filters;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ForbiddenException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import money.management.services.*;

@ComponentScan(basePackages = {"mon	ey.management.services"})
@Component
@Order(1)
public class AuthFilter implements Filter {
	Logger LOG = LoggerFactory.getLogger(AuthFilter.class);
	private static final String TOKEN = "token";
	
	@Autowired
	TokenService tokenService;
	
	@SuppressWarnings("static-access")
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException, ForbiddenException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		String uri = req.getRequestURI();
		LOG.info("URI:" + uri);
		LOG.info("REQUEST METHOD:" + req.getMethod());
		if(uri.startsWith("/data/") && !req.getMethod().equals("OPTIONS")) {
			String token = req.getHeader(TOKEN);
			
			if (token != null && tokenService.validateToken(token) == tokenService.TOKEN_OK) {
				chain.doFilter(request, response);
			} else {
				res.setStatus(401);
				res.setContentType("application/json"); 
				res.getWriter().append(json());
			}
			
		} else {
			chain.doFilter(request, response);	
		}		
	}
	
	private String json() {
		long date = new Date().getTime();
		return "{\"timestamp\": " + date + ", "
				+ "\"status\": 401, "
				+ "\"error\": \"Unauthorized\", "
				+ "\"message\": \"Authentication failed: bad credentials\", "
				+ "}";
	}
}
