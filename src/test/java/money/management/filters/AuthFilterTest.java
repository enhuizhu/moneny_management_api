package money.management.filters;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;

import money.management.services.UrlService;
import money.management.services.TokenService;

@ComponentScan(basePackages = {"money.management.services"})
// @RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AuthFilterTest {
	public static final String TOKEN = "token";
	
	@Autowired
	private TokenService tokenService;
	
	
	@Test
	@Ignore
	public void testDoFilter() throws IOException, ServletException {
		MockHttpServletRequest request = new MockHttpServletRequest();
		AuthFilter authFilter = new AuthFilter();
		String token = tokenService.generateToekn("zhuen2000@163.com");
		request.addHeader(TOKEN, token);
		request.setRequestURI("/data/item/createItem");
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockFilterChain filterChain = new MockFilterChain();
		authFilter.doFilter(request, response, filterChain);
		assertEquals(response.getStatus(), HttpStatus.OK.value());
	}
}
