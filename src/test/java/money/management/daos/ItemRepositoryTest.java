package money.management.daos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@ComponentScan(basePackages = {"money.management.daos"})
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ItemRepositoryTest {
  Logger LOG = LoggerFactory.getLogger(ItemRepositoryTest.class);
  @Autowired
	private ItemRepository itemRepository;
	
	@Test
	public void testCostInDateRange() {
    Item testItem = new Item();
    testItem.setName("TEST");
    Cost cost = new Cost();
    cost.setPayDate(new Date(2019, 10, 1));
    testItem.addCost(cost);
    itemRepository.save(testItem);
    
    List<Cost> costs = testItem.costs(new Date(2018, 1, 1), new Date(2020, 1, 1));
    assertEquals("it should have one cost in the list", 1, costs.size());


    List<Cost> costs2 = testItem.costs(new Date(2020, 1, 1), new Date(2021, 1, 1));
    assertEquals("it should not contain any element in the array", 0, costs2.size());
	}	
}
