package money.management.daos;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@ComponentScan(basePackages = {"money.management.daos"})
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ResetPasswordRepositoryTest {
	@Autowired
	private ResetPasswordRepository resetPasswordRepository;
	
	@Test
	public void testInsertNewRecord() {
		ResetPassword newReset = new ResetPassword();
		newReset.setEmail("test@test.com");
		newReset.setToken("123");
		
		resetPasswordRepository.save(newReset);
		
		ResetPassword reset = resetPasswordRepository.findByToken("123");
		assertNotNull(reset);
		
		// delete it, it should not be found
		resetPasswordRepository.delete(reset);
		assertNull(resetPasswordRepository.findByToken("123"));
	}	
}
