package money.management.services;

import money.management.daos.UserRepository;
import money.management.daos.User;
import money.management.services.TokenService;
import static org.junit.Assert.assertEquals;
import io.jsonwebtoken.Claims;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;

@ComponentScan(basePackages = {"money.management.services"})
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@SpringBootTest
public class TokenServiceTests {
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private UserRepository userDao;
	 

	@Test
	@Ignore
	public void testGenerateToken() {
		String token = tokenService.generateToekn("zhuen2000@163.com");
		System.out.println("token: " + token);
	}
	
	@Test
	@Ignore
	public void testTokenDecode() {
		String token = tokenService.generateToekn("zhuen2000@163.com");
		Claims claims = tokenService.decodeJWT(token);
		assertEquals(claims.getSubject(), "zhuen2000@163.com");
		
		String token2 = tokenService.generateToekn("zheuen20002@163.com");
		Claims claims2= tokenService.decodeJWT(token2);
		assertEquals(claims2.getSubject(), "zheuen20002@163.com");
	}
	
	@Test
	@Ignore
	public void testVolidateToken() {		
		User newUser = new User();
		newUser.setEmail("zhuen2000@163.com");
		newUser.setPassword("123456");
		userDao.save(newUser);
		
		String testToken = "eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJvbG1hcmtldCIsImlhdCI6MTU1ODk2Nzg5NCwic3ViIjoiemh1ZW4yMDAwQDE2My5jb20iLCJpc3MiOiJvbG1hcmtldCIsImV4cCI6MTU1OTA1NDI5NH0.wB6xEF8WG4x1VaxfZW7-AQVphDDnjBu9AwPvmpqhs6Q";
		assertEquals(tokenService.validateToken(testToken), tokenService.TOKEN_EXPIRED);
		
		String freshToken = tokenService.generateToekn("zhuen2000@163.com");
		assertEquals(tokenService.validateToken(freshToken), tokenService.TOKEN_OK);
		
		String wrongToken = tokenService.generateToekn("test@test.com");
		assertEquals(tokenService.validateToken(wrongToken), tokenService.INVALID_TOKEN);
		
	}
}
