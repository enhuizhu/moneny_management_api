package money.management.services;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@ComponentScan(basePackages = {"money.management.services"})
@RunWith(SpringRunner.class)
// @SpringBootTest
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class MailServiceTest {
	@Autowired
	private MailService mailService;
	
	@Test
	@Ignore
	public void sendMail() {
		try {
			mailService.send("sunlilyy@gmail.com", "password reset", "http://localhost/password/reset");	
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
