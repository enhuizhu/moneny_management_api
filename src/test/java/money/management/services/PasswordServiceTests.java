package money.management.services;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import money.management.services.PasswordService;


public class PasswordServiceTests {
	PasswordService passwordService = new PasswordService();
	

	@Test
	public void testEncoder() {
		String result = passwordService.encode("test");
		Boolean isEqual = passwordService.matches("test", result);
		assertTrue(isEqual);
	}

}
