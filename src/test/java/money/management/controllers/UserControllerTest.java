package money.management.controllers;

import static org.hamcrest.Matchers.containsString;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import money.management.daos.User;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private UserController controller;
    
    @LocalServerPort
    private int port;

    @Autowired
    private RestTemplate restTemplate;
    
//    @Autowired
//    private TestRestTemplate restTemplate;
    
//    @Bean
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
    
    @Test
    @Ignore
    public void contexLoads() throws Exception {
        assertThat(controller).isNotNull();
    }
    
    @Test
    @Ignore
    public void shouldReturnDefaultMessage() throws Exception {
    	Map<String, String> user = new HashMap<String, String>();
    	user.put("email", "test5@a.com");
    	user.put("password", "test");
    	
    	Gson gson = new Gson(); 
    	String json = gson.toJson(user); 
    	
    	System.out.print(json);
    	
    	 assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/",
             String.class)).contains("Hello World");
    }
}
