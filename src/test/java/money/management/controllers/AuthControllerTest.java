package money.management.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import money.management.daos.User;
import money.management.daos.Currency;
import money.management.daos.CurrencyRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;

@ComponentScan(basePackages = {"money.management.controllers", "money.management.daos", "money.management.services"})
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class AuthControllerTest {

  @Autowired
  private CurrencyRepository currencyDao;

  @Autowired
  private AuthController controller;

  // controller.currencyDao = currencyDao;
  

  @Test
  public void contexLoads() throws Exception {
    assertNotNull(controller);
  }
  
  @Test
  public void getUserCurrency() {
    User user = new User();
    user.setEmail("zhuen2000@163.com");

    Currency actualCurrency = controller.getUserCurrency(user);
    assertNull(actualCurrency);

    Currency currency = new Currency();
    currency.setName("gbp");
    currencyDao.save(currency);

    Currency actualCurrency2 = controller.getUserCurrency(user);
    assertEquals(currency, actualCurrency2);
  }
}
